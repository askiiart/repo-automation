#!/bin/bash
set -e
apt-get update
apt-get install -y curl
apt-get install -y dpkg-dev

# Update steam - have to do all this stuff because the filename is always the same.
# There's probably a better way to do this.
# Update: This is pretty much just a deb file to install a repo, so it can't be part of my repo
#curl -LO https://cdn.cloudflare.steamstatic.com/client/installer/steam.deb
#if ! cmp --silent "./steam.deb" "/repo/steam.deb"; then
#    curl -L https://cdn.cloudflare.steamstatic.com/client/installer/steam.deb -o /repo/steam.deb
#fi

# ArmCord
cd /repo/dists/bookworm/stable/binary-amd64/
curl -LO $(curl -s https://api.github.com/repos/Legcord/Legcord/releases/latest | grep "browser_download_url.*Legcord-.*-linux-amd64.deb" | cut -d : -f 2,3 | tr -d \") -C -

# Discord
# Has to be manually updated for each version
discord_version="0.0.46"
curl -LO https://dl.discordapp.net/apps/linux/$discord_version/discord-$discord_version.deb -C -

# th-ch/youtube-music
curl -LO $(curl -s https://api.github.com/repos/th-ch/youtube-music/releases/latest | grep "browser_download_url.*youtube-music_.*_amd64.deb" | cut -d : -f 2,3 | tr -d \") -C -

# Vencord Desktop/Vesktop
curl -LO $(curl -s https://api.github.com/repos/Vencord/Vesktop/releases/latest | grep "browser_download_url.*vesktop_.*_amd64.deb" | cut -d : -f 2,3 | tr -d \") -C -

# schildichat-desktop
curl $(curl -s https://api.github.com/repos/SchildiChat/schildichat-desktop/releases/latest | grep "browser_download_url.*schildichat-desktop_.*_amd64.deb" | cut -d : -f 2,3 | tr -d \") -LO -C -

# Do repo stuff
cd /repo
dpkg-scanpackages --arch amd64 dists/bookworm/stable/binary-amd64/ > dists/bookworm/stable/binary-amd64/Packages
cat dists/bookworm/stable/binary-amd64/Packages | gzip -9 > dists/bookworm/stable/binary-amd64/Packages.gz
mv /drone/src/generate-Release.sh ./dists/bookworm/stable/binary-amd64/generate-Release.sh
./dists/bookworm/stable/binary-amd64/generate-Release.sh > Release
