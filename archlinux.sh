#!/usr/bin/env bash
set -x

sudo pacman -Syu --noconfirm --needed

prep() {
    sudo mkdir /var/log/makepkg/
    sudo chown builder /var/log/makepkg/
    chmod -R 777 /var/log/makepkg/
    

    echo "PKGDEST=/repo/
LOGDEST=/var/log/makepkg/" >~/.makepkg.conf
    echo '[multilib]
Include = /etc/pacman.d/mirrorlist

[askiiart]
Server = https://askiiart.net/repos/archlinux/$arch
SigLevel = Optional' | sudo tee -a /etc/pacman.conf
    sudo pacman -Sy
    mkdir /tmp/arch-workdir/
    chmod -R 777 /tmp/arch-workdir/
}

build_aur_package() {
    cd /tmp/arch-workdir/
    mkdir ./$1
    cd ./$1
    zbuilder.sh $1
    rm -rf ~/source
}

prep
cd /tmp/arch-workdir/

git clone https://git.askiiart.net/askiiart/mnt-and-sway
cd ./mnt-and-sway/
makepkg -s -c -C --noconfirm --noprogressbar
cd ..
rm -rf ./mnt-and-sway/

# it doesn't install dependencies from the AUR for some reason (see also python-vdf below)
# python-requests is required for getting the pkgver, at least... for some reason
git clone https://aur.archlinux.org/olympus-bin.git
cd ./olympus-bin/
yay -S lua51-lsqlite3 python-requests pacman-contrib --noconfirm --needed
updpkgsums
makepkg -s -c -C --noconfirm --noprogressbar
cd ..
rm -rf ./olympus-bin/

# these are built from source
build_aur_package 'paru'
build_aur_package 'fjordlauncherunlocked'
build_aur_package 'polycat'
# note that if swayfx depends on the version of scenefx that's currently being built,
# it won't be in the repo as of now, so swayfx will fail
# this is likely, but not a big deal, so i'm not gonna bother with it
build_aur_package 'scenefx'
repo-add -Rp ./askiiart.db.tar.zst $(find . -name "scenefx*.tar.zst" ! -name "askiiart*")
sudo pacman -Sy
build_aur_package 'swayfx'
# note that this requires a *lot* of RAM, more than 16 GB
# can be done on systems with less via zram
build_aur_package 'librewolf-allow-dark'


# these are super tiny so not really any reason not to build them here
build_aur_package 'vesktop-bin'
build_aur_package 'bluetooth-support'
build_aur_package 'rate-mirrors-bin'
build_aur_package 'yay-bin'


# big, so only for slow things and over LAN
build_aur_package 'protonup-qt-bin'
build_aur_package 'schildichat-desktop-bin'
build_aur_package 'visual-studio-code-bin'
build_aur_package 'mindustry-bin'
build_aur_package 'dwarfs-bin'

yay -S python-vdf --noconfirm --needed
build_aur_package 'protontricks'


cd /repo/
rm -rf /tmp/arch-workdir
repo-add -Rp ./askiiart.db.tar.zst $(find . -name "*.tar.zst" ! -name "askiiart*")
